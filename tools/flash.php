<?php 
// MESSAGE FLASH
// affcihe une notifcation 1 fois à l'utilisateur

// ajouter un message flash
// le contenu du message
// le type

function addFlash($type, $content){
    // un tableau associatif qui contient les 2 elements
    $tab = [
        'type' => $type,
        'content' => $content
    ];
    // que je stock dans un tableau en session
    $_SESSION['messages_flash'][] = $tab;
    

    // tab = ["type"]
}

// supprimer tous les messages flash

function remFlash() {
    $_SESSION['messages_flash'] = null;
}
