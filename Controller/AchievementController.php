<?php

class AchievementController
{

    public $database;

    public function __construct()
    {
        // je creer une instance de Database
        // à l'initialisation de Database, 
        // elle creer une nouvelle connexion à la BDD
        $this->database = new Database();
    }

    public function getAchievements()
    {
        $achievements = array();

        $query = "
        SELECT 
            * 
        FROM 
            achievements ";

        $stmt = $this->database->conn->prepare($query);
        $stmt->execute();


        // fecthAll retourne tout les enregistrements sous cette forme
        /*$row = [
            [
                "id" => 1,
                "name" => "php"
            ],
            [
                "id" => 2,
                "name" => "Bootstrap"
            ]
            ];//*/
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $uc = new UserController();

        foreach ($row as $key => $rea) {

            $user = $uc->getUserById($rea['user_id']);
            $achievement = new Achievement();

            $achievement->setId($rea['id']);
            $achievement->setName($rea['name']);
            $achievement->setDescription($rea['description']);
            $achievement->setYear($rea['year']);
            $achievement->setUrl($rea['url']);
            $achievement->setUser($user);
            $achievement->setIsActive($rea['is_active']);

            $achievements[] = $achievement;
        }
        return $achievements;
    }

    public function addAchievement($achievement)
    {
        $query = "
        INSERT INTO 
            `achievements` (
                `id`,
                `name`, 
                `description`, 
                `is_active`, 
                `year`, 
                `url`, 
                `user_id`) 
        VALUES 
            (NULL, :name, :description, :is_active, :year, :url, :user_id);";

        $stmt = $this->database->conn->prepare($query);

        $arguments = [
            ':name' => $achievement->getName(),
            ':description' => $achievement->getDescription(),
            ':is_active' => $achievement->getIsActive(),
            ':year' => $achievement->getYear(),
            ':url' => $achievement->getUrl(),
            ':user_id' => $achievement->getUser()->getId()
        ];
        // var_dump($arguments);

        //$stmt->execute($arguments);
        $stmt->execute($arguments) or die(print_r($stmt->errorInfo(), true));

        echo $this->database->conn->lastInsertId();
    }

    public function updateAchievement($ach)
    {
        $query = "
            UPDATE 
                achievements
            SET 
                `name` = :name, 
                `description` = :description, 
                `is_active` = :is_active, 
                `year` = :year, 
                `url` = :url, 
                `user_id` = :user_id
            WHERE
                id = :id
        ";

        $stmt = $this->database->conn->prepare($query);

        if ($stmt->execute([
            ':id' => $ach->getId(),
            ':name' => $ach->getName(),
            ':description' => $ach->getDescription(),
            ':is_active' => $ach->getIsActive(),
            ':year' => $ach->getYear(),
            ':url' => $ach->getUrl(),
            ':user_id' => $ach->getUser()->getId()
        ])) {
            return "ok";
        } else {
            return $stmt->errorInfo();
        }
    }
}
