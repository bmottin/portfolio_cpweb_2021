<?php

class ExperienceController
{

    public $database;

    public function __construct()
    {
        // je creer une instance de Database
        // à l'initialisation de Database, 
        // elle creer une nouvelle connexion à la BDD
        $this->database = new Database();
    }

    public function getExperiences($onlyActive = true)
    {
        $xps = array();

        $query = "SELECT * FROM `experiences`";

        if ($onlyActive) {
            $query .= " WHERE is_active = :active";

            $stmt = $this->database->conn->prepare($query);
            $stmt->execute([':active' => "1"]);
        } else {

            $stmt = $this->database->conn->prepare($query);
            $stmt->execute();
        }

        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $uc = new UserController();

        foreach ($row as $key => $xp) {

            $user = $uc->getUserById($xp['user_id']);

            $exp = new Experience();
            $exp->setId($xp['id']);
            $exp->setName($xp['name']);
            $exp->setEnterprise($xp['entreprise']);
            $exp->setDateStart($xp['date_start']);
            $exp->setDateEnd($xp['date_end']);
            $exp->setIsCurrent($xp['is_current']);
            $exp->setLocation($xp['location']);
            $exp->setDescription($xp['description']);
            $exp->setUser($user);

            $xps[] = $exp;
        }

        return $xps;
    }

    public function getExperienceById($id)
    {
    }

    public function addExperience($xp)
    {
    }

    public function updateExperience($xp)
    {
    }

    public function deleteExperience()
    {
    }
}
