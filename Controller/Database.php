<?php

class Database
{
    private $server = "localhost";
    private $user = "root";
    private $password = "";
    private $dbname = "portfolio_2021";
    private $port = "3306";

    public $conn;

    public function __construct()
    {
        $this->getConnection();
    }

    public function getConnection()
    {
        // réinitialiser la connection
        $this->conn = null;
        try {
            // je tente la connection à la BDD
            // si ça marche, je me connecte, si une erreur est levée je passe dans le catch
            $this->conn = new PDO("mysql:host=" . $this->server . ";port=" . $this->port . ";dbname=" . $this->dbname, $this->user, $this->password);
            $this->conn->exec("set names utf8");
        } catch (Exception $e) {
            // le catch est appeler que s'il y a une erreur dans le try
            // j'affiche le message d'erreur et son code
            echo "Erreur: " . $e->getMessage() . "(code : " . $e->getCode(). ")";
        } /* finally {
            // je passe tout le temps dedans
            echo "Finally";
        } //*/
        
    }
}
