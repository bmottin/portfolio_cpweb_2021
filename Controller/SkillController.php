<?php

class SkillController
{

    public $database;

    public function __construct()
    {
        // je creer une instance de Database
        // à l'initialisation de Database, 
        // elle creer une nouvelle connexion à la BDD
        $this->database = new Database();
    }

    public function getSkills()
    {
        $skills = array();

        $query = "
        SELECT 
            * 
        FROM 
            skills ";

        $stmt = $this->database->conn->prepare($query);
        $stmt->execute();

        
        // fecthAll retourne tout les enregistrements sous cette forme
        /*$row = [
            [
                "id" => 1,
                "name" => "php"
            ],
            [
                "id" => 2,
                "name" => "Bootstrap"
            ]
            ];//*/
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $uc= new UserController();
        
        foreach ($row as $key => $sk) {

            $user = $uc->getUserById($sk['user_id']);
            $skill = new Skill($sk['id'], $sk['name'], $sk['logo'], $sk['sort_order'],$sk['skill_level'], $user, $sk['is_active']);

            $skills[] = $skill;
        }
        return $skills;
    }

    public function addSkill($skill) {
        $query = "
        INSERT INTO 
            `skills` (
        id, name, is_active, logo, sort_order, skill_level, user_id) 
        VALUES 
            (NULL, :name, 0, :logo, :sort_order, :skill_level, :user_id);";

        $stmt = $this->database->conn->prepare($query);

        $arguments = [
            ':name' => $skill->getName(),
            ':logo' => $skill->getLogo(),
            ':sort_order' => $skill->getSortOrder(),
            ':skill_level' => $skill->getSkillLevel(),
            ':user_id' => $skill->getUser()->getId()
        ];
        var_dump($arguments);

        //$stmt->execute($arguments);
        $stmt->execute($arguments) or die(print_r($stmt->errorInfo(), true));

        echo $this->database->conn->lastInsertId();

    }
}
