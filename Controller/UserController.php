<?php

class UserController
{

    public $database;

    public function __construct()
    {
        // je creer une instance de Database
        // à l'initialisation de Database, 
        // elle creer une nouvelle connexion à la BDD
        $this->database = new Database();
    }

    public function getUserById($id)
    {

        // BDD
        /*
        $firstname = "Harry";
        $lastname = "POTTER";//*/
        // Fin BDD
        // $query = "SELECT * FROM users WHERE id = " . $id;
        $query = "SELECT * FROM users WHERE id = :id";
        // echo $query;
        $stmt = $this->database->conn->prepare($query);
        $stmt->execute([':id' => $id]);
        // retourne les lignes trouvées sous forme de tableau associatif
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($stmt->rowCount() > 0) {


            $user = new User();
            $user->setId($row['id']);
            $user->setFirstname($row['firstname']);
            $user->setLastname($row['lastname']);
            $user->setBiography($row['biography']);
            $user->setAvatar($row['avatar']);
            $user->setPhone($row['phone']);
            $user->setEmail($row['email']);
            $user->setPostalCode($row['postal_code']);
            $user->setCity($row['city']);

            /*
        $user->setId($id);
        
        //*/
            return $user;
        } else {
            echo "Erreur pas d'utilisateur avec cette ID :" . $id;
        }
    }

    public function getUserForLogin($login, $password)
    {
        $query = "SELECT * FROM users WHERE email = :login AND password = :pass";

        $stmt = $this->database->conn->prepare($query);
        $stmt->execute([
            ':login' => $login,
            ':pass' => $password
        ]);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($stmt->rowCount() > 0) {

            $user = new User();
            $user->setId($row['id']);
            $user->setFirstname($row['firstname']);
            $user->setLastname($row['lastname']);
            $user->setBiography($row['biography']);
            $user->setAvatar($row['avatar']);
            $user->setPhone($row['phone']);
            $user->setEmail($row['email']);
            $user->setPostalCode($row['postal_code']);
            $user->setCity($row['city']);

            return $user;
        }
    }

    public function updateUser($user){
        $query = "
            UPDATE 
                users
            SET 
                firstname = :firstname,
                lastname = :lastname,
                biography = :biography
            WHERE
                id = :id
        ";

        $stmt = $this->database->conn->prepare($query);
        
        if($stmt->execute([
            ':firstname' => $user->getFirstname(),
            ':lastname' => $user->getLastname(),
            ':biography' => $user->getBiography(),
            ':id' => $user->getId()
        ])) {
             return "ok";
        } else {
             return $stmt->errorInfo();
        }
        
    }
}
