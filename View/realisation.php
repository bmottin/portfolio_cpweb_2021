<h1>Réalisation</h1>

<?php 

foreach ($achievements as $key => $achievement) {
    ?>
    <article class="border mb-3">
        <h2><?= $achievement->getName() ?></h2>
        <h3><?= $achievement->getYear() ?></h3>
        <p><?= $achievement->getDescription() ?></p>
    </article>
<?php 
}
?>