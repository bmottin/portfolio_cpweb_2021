<h1>Administration</h1>

<div class="row">
    <nav class="col-4">
        <ul>
            <li>
                <a href="?page=admin&subpage=perso" class="btn btn-primary my-4">Gestion Infos Persos</a>
            </li>
            <li>
                <a href="?page=admin&subpage=experience" class="btn btn-primary my-4">Gestion Experience</a>
            </li>
            <li>
                <a href="?page=admin&subpage=achievement" class="btn btn-primary my-4">Gestion Réalisations</a>
            </li>
        </ul>
    </nav>
    <div class="col-7 offset-1">
    
    
