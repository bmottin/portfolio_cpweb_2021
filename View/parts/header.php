<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Portfolio</title>
  <link rel="stylesheet" href="./public/css/bootstrap.min.css">
  <link rel="stylesheet" href="./public/css/style.css">
</head>

<body>


  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="?page=home">Portfolio</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarColor03">
        <ul class="navbar-nav me-auto">
          <li class="nav-item">
            <a class="nav-link active" href="?page=home">Accueil
              <span class="visually-hidden">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <!-- FIXME corriger les liens -->
            <a class="nav-link" href="#xp">Experiences</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#formations">Formations</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#skills">Compétences</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="?page=achievements">Mes réalisations</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="?page=contact">Contact</a>
          </li>
          <!--
          Gestion du site
          -->
          <?php
          if (isset($_SESSION['user'])) {
          ?>
            <li class="nav-item">
              <a class="nav-link" href="?page=admin">Gérer le site</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="?page=action_logout">Se déconnecter</a>
            </li>
          <?php
          } else {
          ?>
            <li class="nav-item">
              <a class="nav-link" href="?page=login">Se connecter</a>
            </li>
          <?php
          }
          ?>
        </ul>
      </div>
    </div>
  </nav>

  <?php
  // je contrôle que j'ai une session en cours et qu'elle 
  // contient un tableau qui s'appelle messages_flash
  if (isset($_SESSION) && isset($_SESSION['messages_flash'])) {

    // je boucle sur mes messages
    foreach ($_SESSION['messages_flash'] as $key => $message) {
  ?>
    <!--  la couleur de la div va sez baser sur le type de message -->
      <div class="alert alert-dismissible alert-<?= $message['type'] ?>">
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>

        <p class="mb-0"><?= $message['content'] ?></p>
      </div>

  <?php

    }
    // une fois tout les messages affichés, je les supprime de la session
    remFlash();
  }
  ?>

  <div class="container">