<div class="row  border">
  <div class="col-12 mt-4">
    <img src="./public/image/<?= $user->getAvatar() ?>" class="rounded-circle" alt="">
  </div>

  <div class="col-12">
    <h1 class="display-4">
      <?= $user->getFirstname() . " " . $user->getLastname() ?>
    </h1>
    <hr class="my-4">
    <div class="row">
      <div class="col-8 offset-2">
        <p><?= $user->getBiography() ?></p>
        <div class="row">
          <div class="col-12 d-flex justify-content-center my-4">
            <a class="btn btn-primary btn-lg" href="#" role="button">Contactez-moi !</a>
          </div>
        </div>


      </div>

    </div>

  </div>

  <div>
    <?= /* $user->getPhone() . " " . $user->getPostalCode() . " " . $user->getCity()  . " " . $user->getEmail() */ " " ?>
  </div>

</div>

<div class="row">
  <section id="xp">
    <h2 >Expériences</h2>
    <div class="card-group">
      <?php
      foreach ($experiences as $key => $xp) {
      ?>
        <div class="card m-2" style="width: 18rem;">
          <div class="card-body">
            <h5 class="card-title"><?= $xp->getName() ?></h5>
            <h6 class="card-subtitle mb-2 text-muted">
              <!-- j'affiche l'entreprise -->
              <?= $xp->getEnterprise() ?>
              -
              <!-- j'affiche la date de début de la prise de poste -->
              <?= $xp->getDateStart() ?>
              ➔
              <!-- j'affiche la fin  -->
              <?php
              if ($xp->getIsCurrent()) {
                echo "En cours";
              } else {
                echo $xp->getDateEnd();
              }
              ?>

            </h6>
            <p class="card-text"><?= $xp->getDescription() ?></p>
            <a href="?page=realisation" class="card-link">truc fait</a>
            <a href="#" class="card-link">Another link</a>
          </div>
        </div>

      <?php
      }
      ?>
    </div>

  </section>
</div>

<div class="row">
  <section id="formations">
    <h2>Formation</h2>
    <!-- Afficher compétences -->
    <ul>
      <?php
      // FIXME mettre la liste des formations
      foreach ($skills as $key => $skill) {
        echo "<li>" . $skill->getName() . "</li>";
      }
      ?>
    </ul>
  </section>
</div>


<div class="row">
  <section id="skills">
    <h2>Compétences</h2>
    <!-- Afficher compétences -->
    <ul>
      <?php
      foreach ($skills as $key => $skill) {
        echo "<li>" . $skill->getName() . "</li>";
      }
      ?>
    </ul>
  </section>
</div>