<h2>Gestion Réalisation</h2>
<h3>Ajout de réalisation</h3>

<form action="?page=action_achievement_add" method="post">
    <input type="text" name="name" placeholder="Nom du projet" class="form-control my-2">

    <input type="date" name="year" placeholder="jj/mm/aaaa" class="form-control my-2">

    <textarea name="description" placeholder="Description du projet" class="form-control my-2" id="" cols="30" rows="10"></textarea>

    <input type="url" name="url" placeholder="URL" class="form-control my-2" id="">

    <label for="is_active" class="form-check-label">est à publier ?
        <input type="checkbox" name="is_active" value="1" class="form-check-input" id="is_active">
    </label><br>

    <!-- FIXME rendre dynamique l'id de l'utilisateur -->
    <input type="hidden" name="user_id" value="1">

    <input type="submit" value="Ajouter" class="btn btn-primary my-2">
</form>

<hr>
<h3>Mettre à jour</h3>

<h4>Sommaire</h4>

<ul>
    <?php
    foreach ($achievements as $key => $list) {
        ?>
        <li><a href="#<?= $list->getId() ?>"><?= $list->getName() ?></a></li>
    <?php            
    }
    ?>
    
</ul>

<?php

foreach ($achievements as $key => $ach) {
?>

    <fieldset class="border mb-2 p-2" id="<?= $ach->getId() ?>">
        <legend><?= $ach->getName() ?></legend>
        <form action="?page=action_achievement_update" method="post">
            <input type="text" name="name" value="<?= $ach->getName() ?>" placeholder="Nom du projet" class="form-control my-2">

            <input type="date" name="year" value="<?= $ach->getYear()  ?>" placeholder="jj/mm/aaaa" class="form-control my-2">

            <textarea name="description" placeholder="Description du projet" class="form-control my-2" id="" cols="30" rows="10"><?= $ach->getDescription() ?> </textarea>

            <input type="url" name="url" value="<?= $ach->getUrl()  ?>" placeholder="URL" class="form-control my-2" id="">

            <label for="is_active" class="form-check-label">est à publier ?
                <input type="checkbox" name="is_active" value="<?= $ach->getIsActive() ?>" class="form-check-input" id="is_active" <?php
                                                                                                                                    if ($ach->getIsActive()) {
                                                                                                                                        echo "checked";
                                                                                                                                    }
                                                                                                                                    ?>>
            </label><br>

            <!-- FIXME rendre dynamique l'id de l'utilisateur -->
            <input type="hidden" name="user_id" value="1">
            <input type="hidden" name="achievement_id" value="<?= $ach->getId() ?>">

            <input type="submit" value="Mettre à jour" class="btn btn-primary my-2">
        </form>
    </fieldset>

<?php
}

?>