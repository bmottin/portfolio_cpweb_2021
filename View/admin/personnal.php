<h2>Infos Personnelles</h2>

<form action="?page=action_user_update" method="post">
    <input type="text" name="firstname" placeholder="Prénom" class="form-control my-2">
    <input type="text" name="lastname" placeholder="Nom" class="form-control my-2">
    <textarea name="biography" placeholder="Biographie" class="form-control my-2" id="" cols="30" rows="10"></textarea>

    <input type="submit" value="Mettre à jour" class="btn btn-primary my-2">
</form>