<?php
session_start();
// session_destroy();
// echo "<pre>";

// fichier du "moteur"
require_once "Model/User.php";
require_once "Model/Skill.php";
require_once "Model/Achievement.php";
require_once "Model/Studie.php";
require_once "Model/Experience.php";

require_once "Controller/UserController.php";
require_once "Controller/SkillController.php";
require_once "Controller/AchievementController.php";
require_once "Controller/ExperienceController.php";
require_once "Controller/Database.php";

require_once "tools/flash.php";
/*
addFlash("success", "OK");
addFlash("warning", "meh");
addFlash("danger", "ko");
addFlash("info", "test");
//*/

/*
 * ROUTAGE
 * */

// je contrôle quelle page est demandée
// si l'argument GET page existe, 
// c'est cette page que je cherche
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    // sinon j'affiche la page d'accueil par défaut
    $page = "home";
}

// afficher le menu et le head de notre page
require_once "View/parts/header.php";

// Début Logique du moteur
switch ($page) {
    case 'home':
        $uc = new UserController();
        $user = $uc->getUserById(1);
        // var_dump($user);

        $usk = new SkillController();
        $skills = $usk->getSkills();

        $xpc = new ExperienceController();
        $experiences = $xpc->getExperiences(false);

        require_once "View/home.php";
        break;
    case "contact":
        require_once "View/contact.php";
        break;

    case "achievements":
        $ac = new AchievementController();
        $achievements = $ac->getAchievements();
        // echo "<pre>";
        // var_dump($achievements);
        require_once "View/realisation.php";
        break;

    case "login":
        require_once "View/login.php";
        break;



    case "admin":
        /*
         * ADMIN
         * */
        // affiche le sous-menu administration
        require_once "View/parts/admin_menu.php";

        if (isset($_GET['subpage'])) {
            $subpage = $_GET['subpage'];
        } else {
            $subpage = "perso";
        }

        /* 
         * Gestion des sous parties de l'admin
         * */
        switch ($subpage) {
            case 'perso':
                require_once "View/admin/personnal.php";
                break;
            case 'experience':
                require_once "View/admin/experiences.php";
                break;
            case 'achievement':
                // je creer une nouvelle instance de achievement controller
                // et je recuperere la liste de mes achievements
                $ac = new AchievementController();
                $achievements = $ac->getAchievements();

                require_once "View/admin/achievements.php";
                break;
            default:
                require_once "View/404.php";
                break;
        }
        require_once "View/parts/admin_menu_end.php";

        break;
        /*
         * ACTIONS
         * */
    case 'action_login':
        require_once "Action/proceed_login.php";
        break;
    case "action_logout":
        require_once "Action/proceed_logout.php";
        break;
    case "action_user_update":
        require_once "Action/proceed_user_update.php";
        break;
    case "action_achievement_add":
        require_once "Action/proceed_achievement_add.php";
        break;
    case "action_achievement_update":
        require_once "Action/proceed_achievement_update.php";
        break;
    default:
        require_once "View/404.php";
        break;
}

// Fin logique moteur

require_once "View/parts/footer.php";
