<?php

if(isset($_POST)) {

    // je recupere l'utilisateur connecté
    $uc = new UserController();
    $user2Update = $uc->getUserById($_SESSION['user_id']);

    // je ne met à jour que les champs modifiés
    if(isset($_POST['firstname']) && strlen($_POST['firstname']) > 0)  {
        $user2Update->setFirstname(htmlspecialchars($_POST['firstname']));
    }

    if(isset($_POST['lastname'])  && strlen($_POST['lastname']) > 0) {
        $user2Update->setLastname(htmlspecialchars($_POST['lastname']));
    }

    if(isset($_POST['biography'])  && strlen($_POST['biography']) > 0) {
        $user2Update->setBiography(htmlspecialchars($_POST['biography']));
    }

    // puis je met à jour la BDD (qu'une fois)
    $uc->updateUser($user2Update);

    header("location:?page=admin");
}