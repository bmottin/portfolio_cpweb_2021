<?php

if ($_POST) {
    if ($_POST['name'] && $_POST['is_active'] && $_POST['year'] && $_POST['user_id']) {
        $uc = new UserController();
        $user = $uc->getUserById($_POST['user_id']);

        $achievement = new Achievement();
        $achievement->setName($_POST['name']);
        if(isset($_POST['is_active'])) {
            $achievement->setIsActive(1);
        } else {
            // sinon il n'apparait même pas dans la requete POST, donc vaut 0
            $achievement->setIsActive(0);
        }
        $achievement->setUser($user);
        $achievement->setYear($_POST['year']);

        if ($_POST['description']) {
            $achievement->setDescription($_POST['description']);
        } else {
            $achievement->setDescription(null);
        }

        if ($_POST['url']) {
            $achievement->setUrl($_POST['url']);
        } else {
            $achievement->setUrl(null);
        }

        $ac = new AchievementController();
        $ac->addAchievement($achievement);

        addFlash("success", "Ajout réussi");
        header("location:?page=admin&subpage=achievement");
    }
} else {
    addFlash("danger", "action interdite");
}

