<?php

var_dump($_POST);

if ($_POST) {
    if ($_POST['name'] && $_POST['year'] && $_POST['user_id'] && $_POST['achievement_id']) {
        $uc = new UserController();
        $user = $uc->getUserById($_POST['user_id']);

        $achievement = new Achievement();
        $achievement->setName($_POST['name']);
        // si is_active est coché, alors il vaut 1
        if(isset($_POST['is_active'])) {
            $achievement->setIsActive(1);
        } else {
            // sinon il n'apparait même pas dans la requete POST, donc vaut 0
            $achievement->setIsActive(0);
        }
        $achievement->setUser($user);
        $achievement->setYear($_POST['year']);
        $achievement->setId($_POST['achievement_id']);

        if ($_POST['description']) {
            $achievement->setDescription($_POST['description']);
        } else {
            $achievement->setDescription(null);
        }

        if ($_POST['url']) {
            $achievement->setUrl($_POST['url']);
        } else {
            $achievement->setUrl(null);
        }

        $ac = new AchievementController();
        $ac->updateAchievement($achievement);

        addFlash("success", "Modification réussi pour la réalisation " . $achievement->getName());

        header("location:?page=admin&subpage=achievement");
    }
} else {
    addFlash("danger", "action interdite");
}

