<?php

class User {
    
    private $id;
    private $firstname;
    private $lastname;
    private $avatar;
    private $biography;
    private $email;
    private $phone;
    private $password;
    private $city;
    private $postalCode;

    private $skills;

    public function getName(){
        return $this->firstname . " " . $this->lastname;
    }

    public function getFormatedLocation() {
        return "<h2>" .$this->postalCode . " " . $this->city . "</h2>";
    }

    public function __construct()
    {
        $this->skills = array();
    }

    public function __toString()
    {
        return "<h2>😈" . $this->firstname . " | " . $this->lastname . "😈</h2>";
    }

    /*
        ACCESSEUR
    */

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of avatar
     */ 
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set the value of avatar
     *
     * @return  self
     */ 
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get the value of firstname
     */ 
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set the value of firstname
     *
     * @return  self
     */ 
    public function setFirstname($firstname)
    {
        if(strlen($firstname)>0) {
            $this->firstname = $firstname;
        } else {
            echo "Erreur, le prenom doit avoir au moins 1 car";
        }
        return $this;
    }

    /**
     * Get the value of lastname
     */ 
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set the value of lastname
     *
     * @return  self
     */ 
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get the value of biography
     */ 
    public function getBiography()
    {
        return $this->biography;
    }

    /**
     * Set the value of biography
     *
     * @return  self
     */ 
    public function setBiography($biography)
    {
        $this->biography = $biography;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of phone
     */ 
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the value of phone
     *
     * @return  self
     */ 
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */ 
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of city
     */ 
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     * @return  self
     */ 
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get the value of postalCode
     */ 
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set the value of postalCode
     *
     * @return  self
     */ 
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get the value of skills
     */ 
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * Add skill to the User's Skills array
     */
    public function addSkill($skill) {
        array_push($this->skills, $skill);
    }

    /**
     * Remove the submited skill from the user's Skills array
     */
    public function remSkill($skill) {
        $i = 0; // index à 0
        foreach ($this->skills as $key => $sk) {
            // pour chaque je vérifie 
            // si le skill correspond au skill à supprimer
            if($sk->getId() == $skill->getId()) {
                // si oui je supprime
                unset($this->skills[$i]);
                break; // sort de la boucle
            }

            // sinon j'incrémente l'index
            $i++;
        }
    }
}