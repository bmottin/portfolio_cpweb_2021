<?php

class Studie
{
    private $id;
    private $name;
    private $studieLevel;
    private $dateStart;
    private $school;
    private $dateEnd;
    private $isGraduate;
    private $description;
    private $user;
    private $isActive;

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of studieLevel
     */ 
    public function getStudieLevel()
    {
        return $this->studieLevel;
    }

    /**
     * Set the value of studieLevel
     *
     * @return  self
     */ 
    public function setStudieLevel($studieLevel)
    {
        $this->studieLevel = $studieLevel;

        return $this;
    }

    /**
     * Get the value of dateStart
     */ 
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set the value of dateStart
     *
     * @return  self
     */ 
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get the value of dateEnd
     */ 
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set the value of dateEnd
     *
     * @return  self
     */ 
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get the value of isGraduate
     */ 
    public function getIsGraduate()
    {
        return $this->isGraduate;
    }

    /**
     * Set the value of isGraduate
     *
     * @return  self
     */ 
    public function setIsGraduate($isGraduate)
    {
        $this->isGraduate = $isGraduate;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of user
     */ 
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @return  self
     */ 
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get the value of school
     */ 
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Set the value of school
     *
     * @return  self
     */ 
    public function setSchool($school)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get the value of isActive
     */ 
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set the value of isActive
     *
     * @return  self
     */ 
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }
}
