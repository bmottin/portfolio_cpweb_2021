-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : lun. 14 juin 2021 à 10:08
-- Version du serveur :  5.7.24
-- Version de PHP : 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `portfolio_2021`
--

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `avatar`, `biography`, `email`, `phone`, `password`, `city`, `postal_code`) VALUES
(1, 'Benoit', 'MOTTIN', NULL, NULL, 'benoit.mottin@normandie.cci.fr', '02.33.02.02.02', 'admin123', 'Saint Lô', '50000');


--
-- Déchargement des données de la table `achievements`
--

INSERT INTO `achievements` (`id`, `name`, `description`, `is_active`, `year`, `url`, `user_id`) VALUES
(2, 'Portfolio', NULL, 0, '2021-06-01', NULL, 1);

--
-- Déchargement des données de la table `experiences`
--

INSERT INTO `experiences` (`id`, `name`, `entreprise`, `is_active`, `date_start`, `date_end`, `is_current`, `location`, `description`, `user_id`) VALUES
(1, 'Fimbucks', 'FIM IT', 0, '2020-05-01', NULL, 1, 'Saint Lô', 'Création d\'un site pour la chaine de café FimBucks', 1);

--
-- Déchargement des données de la table `skills`
--

INSERT INTO `skills` (`id`, `name`, `is_active`, `logo`, `sort_order`, `skill_level`, `user_id`) VALUES
(1, 'PHP', 0, 'php', 1, 4, 1),
(2, 'Bootstrap', 0, 'bt', 2, 4, 1);

--
-- Déchargement des données de la table `studies`
--

INSERT INTO `studies` (`id`, `name`, `is_active`, `studies_level`, `date_start`, `date_end`, `is_graduate`, `school`, `description`, `user_id`) VALUES
(1, 'Chef de projet Webmarketing et Conception de site internet', 0, '7', '2020-06-01', NULL, 0, 'FIM CCI Formation', 'Je fait des sites et des campagnes web marketing avec une touche de SEO.', 1);


COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
