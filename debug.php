<?php

//declare(strict_types=1);

// import des dépendances
require_once "Model/User.php";
require_once "Model/Experience.php";
require_once "Model/Achievement.php";
require_once "Model/Skill.php";
require_once "Model/Studie.php";

require_once "Controller/Database.php";
require_once "Controller/UserController.php";
require_once "Controller/SkillController.php";
require_once "Controller/ExperienceController.php";

/* 
    USER
*/

$user = new User();
$user->setId(1);
$user->setFirstname("Benoit");
$user->setLastname("MOTTIN");
$user->setAvatar("MOTTIN.png");
$user->setBiography("Bla bla bla");
$user->setEmail("benoit.mottin@bmla.com");
$user->setPassword("admin123");
$user->setPhone("02.33.02.02.02");
$user->setPostalCode("50000");
$user->setCity("Saint Lô");

echo $user->getFirstname();

echo "<pre>";
var_dump($user);

echo $user->getName();
echo $user->getFormatedLocation();

echo "<hr>";
echo "<h2>XP</h2>";

$xp = new Experience();
$xp->setId(1);
$xp->setName("Developpeur");
$xp->setDateStart(new DateTime("2020-01-10"));
$xp->setIsCurrent(true);
$xp->setEnterprise("FIM");
$xp->setDescription("Je fais des sites");
$xp->setLocation("254 rue Lycette Darsonval 50000 Saint Lô");
$xp->setUser($user);

var_dump($xp);

echo $xp->getUser()->getName();

echo "<hr>";
echo "<h2>Réalisation</h2>";

$real = new Achievement(1, "Fimbuck", "site pour la Soc. Fimbuck", new DateTime("2020-01-01"), "", $user);

var_dump($real);

$real2 = new Achievement(2, "Portfolio", "");

$real2->setDescription("Blabla");


echo "<hr>";
echo "<h2>Skill</h2>";
// TODO Ajouter une collection de skills dans le User
$sk = new Skill(1, "PHP", "php", 3, 4, $user);
$sk2 = new Skill(2, "HTML", "html5", 1, 3, $user);
$sk3 = new Skill(2, "Bootstrap", "bt", 2, 3, $user);

$user->addSkill($sk);
$user->addSkill($sk3);
$user->addSkill($sk2);

echo "<ul>";
foreach ($user->getSkills() as $key => $skill) {
    echo "<li>" . $skill->getName() . "</li>";
}
echo "</ul>";

$user->remSkill($sk);

var_dump($user);



echo "<hr>";
echo "<h2>Studie</h2>";

$st = new Studie();
$st->setId(1);
$st->setName("CPWEB");
$st->setDateStart(new DateTime("2020-06-12"));
$st->setDateEnd(new DateTime("2021-10-12"));
$st->setSchool("FIM Formation");
$st->setIsGraduate(false);
$st->setDescription("Je fait des sites");
$st->setUser($user);

echo "<hr>";
echo "<h1>Controller</h1>";

$uc = new UserController();
$user2 = $uc->getUserById(1);

echo $user2;


echo "<hr>";
echo "<h2>SkillController</h2>";

$sc = new SkillController();
foreach ($sc->getSkills() as $key => $skill) {
    echo $skill->getName() . "<br>";
}

echo "<hr>Debug";
$sk4 = new Skill(null, "Debugger", "bug", 4, 4, $user2);

var_dump($sk4);
//$sc->addSkill($sk4);

echo "<hr>";
echo "<h2>ExperienceController</h2>";

$exc = new ExperienceController();
foreach ($exc->getExperiences() as $key => $xp) {
    echo $xp->getName() . " " . $xp->getDateStart() . " " . $xp->getDateEnd() . "<br>";
}
